#include "MollerTromboreOption.h"
#include "Application.h"
#include "Object.h"
#include <iostream>
#include "MollerTrumbore.h"

MollerTromboreOption::MollerTromboreOption()
{

}

MollerTromboreOption::MollerTromboreOption(std::string displayText, std::string outputText) : Option(displayText, outputText)
{

}

MollerTromboreOption::~MollerTromboreOption()
{

}

void MollerTromboreOption::action()
{
	if (Application::context->object != NULL)
	{
		MollerTrumbore::run();
	}
	else
		Application::context->outputText = "A valid object has not been loaded.";
}