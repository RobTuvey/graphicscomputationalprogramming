#include "MollerTromboreLogsOption.h"
#include "Application.h"
#include <iostream>

MollerTromboreLogsOption::MollerTromboreLogsOption()
{

}

MollerTromboreLogsOption::MollerTromboreLogsOption(std::string displayText, std::string outputText) : Option(displayText, outputText)
{

}

MollerTromboreLogsOption::~MollerTromboreLogsOption()
{

}

void MollerTromboreLogsOption::action()
{
	if (!Application::context->mollerTrumboreDirty)
	{
		std::vector<std::string> log = Application::context->mollerTrumboreLog;

		for (size_t i = 0; i < log.size(); i++)
		{
			std::string logCurrent = log.at(i);
			std::cout << logCurrent.c_str() << std::endl;
		}

		std::cin.clear();
		std::cin.ignore(100, '\n');
		std::string line;
		std::getline(std::cin, line);
		Application::context->outputText = "";
	}
	else
		Application::context->outputText = "Moller-Trumbore logs are not up to date or incomplete.";
}