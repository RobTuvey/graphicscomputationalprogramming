#include "BruteForce.h"
#include "Common.h"
#include "Ray.h"
#include "Application.h"
#include "Object.h"
#include "Mathf.h"
#include "MollerTrumbore.h"
#include "Vector3d.h"
#include <iostream>
#include "Clock.h"
#include <string>

void BruteForce::run()
{
	Clock::takeTime();//Take measurement for calculating processing time of operation.

	Application::context->bruteForceLog.clear();
	int intersections = 0;
	int parallels = 0;
	std::vector<shared<Ray>> rays = Ray::generateRays();
	std::vector<std::vector<int>> objTriangles = Application::context->object->getTriangles();
	std::vector<Vector3> vertices = Application::context->object->getVertices();

	for (size_t i = 0; i < objTriangles.size(); i++)
	{
		std::cout << "Triangle " << i << std::endl;
		//Retrieves triangle vertices from list of vertices loaded.
		std::vector<int> triangle = objTriangles.at(i);
		Vector3 A = vertices.at(triangle.at(0));
		Vector3 B = vertices.at(triangle.at(1));
		Vector3 C = vertices.at(triangle.at(2));

		std::vector<Vector3> vecTriangle;
		vecTriangle.push_back(A);
		vecTriangle.push_back(B);
		vecTriangle.push_back(C);
		//Calculates coordinates of the centre of the triangle.
		Vector3 triCentre = Vector3((A.x + B.x + C.x) / 3, (A.y + B.y + C.y) / 3, (A.z + B.z + C.z) / 3);
		//Defines edge vectors of the triangle.
		Vector3 edge1 = B - A;
		Vector3 edge2 = C - A;
		//Calculates surface normal of the 2 edges.
		Vector3d planePart = Mathf::cross(edge1, edge2);
		//Returns normalized vector representing surface normal of supporting plane.
		Vector3d planeNormal = planePart / Mathf::magnitude(planePart);
		//Dot product used for checking if the triangle is facing the ray origin.
		double dotProd = Mathf::dot(Application::context->cameraPos, planeNormal);
		//Returns resulting float representing the supporting plane of the triangle.
		double planeFloat = Mathf::dot(planeNormal, A);
		//If back culling is enabled then the dot product of the ray origin and the plane
		//Normal must be less than 0 otherwise it means that it is not facing the ray origin.
		if ((Application::context->backCulling && dotProd < 0) || !Application::context->backCulling)
		{
			for (size_t j = 0; j < rays.size(); j++)
			{

				Vector3 intersectionPoint = Vector3d(0);
				Ray ray = *rays.at(j);
				//Computes the value of the t component of the ray. 
				//Using the rearranged equation for the plane with the ray position
				//Substituted for the value on the plane.
				double dot1 = Mathf::dot(planeNormal, ray.getStartVector());
				double dot2 = Mathf::dot(planeNormal, ray.getDirectionVector());
				double t = (planeFloat - dot1) / dot2;
				//Determines the intersection point of the ray and the plane using the computed t component.
				intersectionPoint = ray.getStartVector() + (ray.getDirectionVector() * t);
				//Dot product to determine if the ray and plane are parallel.
				double parallelCheck = Mathf::dot(ray.getDirectionVector(), planeNormal);
				//Only checks the intersection if it is known the ray is not parallel with the plane.
				if (t > 0 && parallelCheck != 0 && intersectsTriangle(intersectionPoint, vecTriangle, planeNormal))
				{
					intersections++;
					
					std::string log = "--Ray " + std::to_string(j) +
						" intersects Triangle " + std::to_string(i) + " at " +
						std::to_string(intersectionPoint.x) + ", " + std::to_string(intersectionPoint.y) +
						", " + std::to_string(intersectionPoint.z);
					std::cout << log << std::endl;
					Application::context->bruteForceLog.push_back(log);
				}
				else if (parallelCheck == 0)
				{
					std::cout << "--Ray " << j << " is parallel with triangle " << i << std::endl;
				}

			}
		}
	}

	Application::context->bruteForceLog.push_back("\n");
	Application::context->outputText =
		"Intersections detected: " + std::to_string(intersections) +
		"\nFaces: " + std::to_string(objTriangles.size()) +
		"\nRays: " + std::to_string(rays.size()) +
		"\nTime Taken: " + std::to_string(Clock::getResult()) + " s" +
		"\nIntersection points displayed above.";
	Application::context->bruteForceLog.push_back(Application::context->outputText);

	Application::context->outputOffset = 5;

	Application::context->bruteForceDirty = false;
}

bool BruteForce::intersectsTriangle(Vector3 intersectionPoint, std::vector<Vector3> triangle, Vector3 planeNormal)
{
	Vector3 A = triangle.at(0);
	Vector3 B = triangle.at(1);
	Vector3 C = triangle.at(2);
	//Retrieves the normal from each edge and vector from intersection point
	//And corresponding poing. And the resulting dot product determines if 
	//It is moving in the same direction as the normal.
	Vector3d crossBA = Mathf::cross((B - A), (intersectionPoint - A));
	double dotBA = Mathf::dot(crossBA, planeNormal);
	if (dotBA < 0)
		return false;

	Vector3d crossCB = Mathf::cross((C - B), (intersectionPoint - B));
	double dotCB = Mathf::dot(crossCB, planeNormal);
	if (dotCB < 0)
		return false;

	Vector3d crossAC = Mathf::cross((A - C), (intersectionPoint - C));
	double dotAC = Mathf::dot(crossAC, planeNormal);
	if (dotAC < 0)
		return false;
	//When processed in a counter clockwise direction if all three tests
	//Pass then the intersection point must be within the triangle.
	return true;
}