#include "BackCullingOption.h"
#include "Application.h"

BackCullingOption::BackCullingOption()
{

}

BackCullingOption::BackCullingOption(std::string displayText, std::string outputText) : Option(displayText, outputText)
{

}

BackCullingOption::~BackCullingOption()
{

}

void BackCullingOption::action()
{
	if (Application::context->backCulling)
	{
		setDisplayText("Enable Back-Face Culling.");
		Application::context->backCulling = false;
		Application::context->outputText = "Back-Face Culling Disabled.";
	}
	else
	{
		setDisplayText("Disable Back-Face Culling.");
		Application::context->backCulling = true;
		Application::context->outputText = "Back-Face Culling Enabled.";
	}
}