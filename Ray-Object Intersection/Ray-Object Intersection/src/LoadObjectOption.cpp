#include "LoadObjectOption.h"
#include "Application.h"
#include "FileManager.h"
#include "Object.h"
#include <iostream>

LoadObjectOption::LoadObjectOption()
{

}

LoadObjectOption::LoadObjectOption(std::string displayText, std::string outputText) : Option(displayText, outputText)
{

}

LoadObjectOption::~LoadObjectOption()
{

}

void LoadObjectOption::action()
{
	char file[100];

	ZeroMemory(&objFile, sizeof(objFile));
	objFile.lStructSize = sizeof(objFile);
	objFile.hwndOwner = NULL;
	objFile.lpstrFile = file;
	objFile.lpstrFile[0] = '\0';
	objFile.nMaxFile = sizeof(file);
	objFile.lpstrFilter = "All\0*.*\0Object\0*.OBJ\0";
	objFile.nFilterIndex = 1;
	objFile.lpstrFileTitle = NULL;
	objFile.nMaxFileTitle = 0;
	objFile.lpstrInitialDir = NULL;
	objFile.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	if (GetOpenFileName(&objFile))
	{
		Application::context->object = FileManager::loadObj(objFile.lpstrFile);
		if (Application::context->object != NULL)
		{
			Application::context->outputText = "Object successfully loaded.";
			Application::context->bruteForceDirty = true;
			Application::context->mollerTrumboreDirty = true;
		}
		else
			Application::context->outputText = "Object loading failed.";
	}
}