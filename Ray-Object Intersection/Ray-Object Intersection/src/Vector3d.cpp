#include "Vector3d.h"
#include "Vector3.h"

Vector3d::Vector3d()
{

}

Vector3d::Vector3d(double x, double y, double z)
{
	this->x = x;
	this->y = y;
	this->z = z;

}

Vector3d::Vector3d(Vector3 v)
{
	this->x = v.x;
	this->y = v.y;
	this->z = v.z;
}

Vector3d::~Vector3d()
{

}

Vector3d Vector3d::operator+(Vector3d v)
{
	return Vector3d(x + v.x, y + v.y, z + v.z);
}

Vector3d Vector3d::operator-(Vector3d v)
{
	return Vector3d(x - v.x, y - v.y, z - v.z);
}

Vector3d Vector3d::operator*(Vector3d v)
{
	return Vector3d(x * v.x, y * v.y, z * v.z);
}

Vector3d Vector3d::operator+(double d)
{
	return Vector3d(x + d, y + d, z + d);
}

Vector3d Vector3d::operator-(double d)
{
	return Vector3d(x - d, y - d, z - d);
}

Vector3d Vector3d::operator*(double d)
{
	return Vector3d(x * d, y * d, z * d);
}

Vector3d Vector3d::operator/(double d)
{
	return Vector3d(x / d, y / d, z / d);
}

Vector3d Vector3d::operator+=(Vector3d v)
{
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;

	return *this;
}

Vector3d Vector3d::operator=(Vector3d* v)
{
	this->x = v->x;
	this->y = v->y;
	this->z = v->z;

	return *this;
}

Vector3d Vector3d::operator=(Vector3d v)
{
	this->x = v.x;
	this->y = v.y;
	this->z = v.z;

	return *this;
}

Vector3d Vector3d::operator=(Vector3 v)
{
	this->x = v.x;
	this->y = v.y;
	this->z = v.z;

	return *this;
}