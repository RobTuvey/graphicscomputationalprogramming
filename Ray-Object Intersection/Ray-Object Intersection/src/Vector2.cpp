#include "Vector2.h"

Vector2::Vector2()
{
	x = 0.0f;
	y = 0.0f;
}

Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
}

Vector2::Vector2(float f)
{
	x = f;
	y = f;
}

Vector2 Vector2::operator+ (Vector2 v)
{
	return Vector2(x + v.x, y + v.y);
}
Vector2 Vector2::operator- (Vector2 v)
{
	return Vector2(x - v.x, y - v.y);
}
Vector2 Vector2::operator* (Vector2 v)
{
	return Vector2(x * v.x, y * v.y);
}
Vector2 Vector2::operator+ (float f)
{
	return Vector2(x + f, y + f);
}
Vector2 Vector2::operator- (float f)
{
	return Vector2(x - f, y - f);
}
Vector2 Vector2::operator* (float f)
{
	return Vector2(x * f, y * f);
}
Vector2 Vector2::operator/ (float f)
{
	return Vector2(x / f, y / f);
}
Vector2 Vector2::operator+= (Vector2 v)
{
	this->x += v.x;
	this->y += v.y;
	return *this;
}
Vector2 Vector2::operator= (Vector2* v)
{
	this->x = v->x;
	this->y = v->y;
	return *this;
}
Vector2 Vector2::operator= (Vector2 v)
{
	this->x = v.x;
	this->y = v.y;
	return *this;
}