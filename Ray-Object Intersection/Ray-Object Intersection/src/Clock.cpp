#include "Clock.h"

std::chrono::time_point<std::chrono::high_resolution_clock> Clock::measurePoint;

void Clock::takeTime()
{
	measurePoint = std::chrono::high_resolution_clock::now();
}

float Clock::getResult()
{
	using namespace std::chrono;
	
	time_point<high_resolution_clock> now = high_resolution_clock::now();
	duration<double> measuredTime = duration_cast<duration<double>>(now - measurePoint);

	return measuredTime.count();
}