#include "QuitOption.h"
#include "Application.h"
#include <iostream>

QuitOption::QuitOption()
{

}

QuitOption::QuitOption(std::string displayText, std::string outputText) : Option(displayText, outputText)
{

}

QuitOption::~QuitOption()
{

}

void QuitOption::action()
{
	Application::context->quit = true;
	Application::context->outputText =  getOutputText();
}