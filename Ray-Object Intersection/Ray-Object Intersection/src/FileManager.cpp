#include "FileManager.h"
#include "Object.h"
#include "Vector3.h"
#include "Vector2.h"
#include <fstream>
#include <sstream>
#include <iterator>

shared<Object> FileManager::loadObj(std::string filename)
{
	std::string line = "";
	std::ifstream file(filename);
	std::istringstream ss;

	std::vector<Vector3> verticesLoaded;
	std::vector<Vector2> UVsLoaded;
	std::vector<Vector3> normalsLoaded;

	std::vector<std::vector<int>> triangles;

	std::vector<unsigned int> vertexIDs;

	if (file.is_open())
	{
		while (getline(file, line))
		{
			ss.clear();
			std::string header = line.substr(0, 2);

			if (header == "v ")
			{
				Vector3 v;
				ss.str(line.substr(2));
				ss >> v.x >> v.y >> v.z;
				verticesLoaded.push_back(v);
			}
			else if (header == "f ")
			{
				ss.str(line.substr(2));
				std::istream_iterator<std::string> beg(ss), end;
				std::vector<std::string> fparts(beg, end);
				ss.clear();

				std::vector<int> triangleVerts;

				for each(std::string fstring in fparts)
				{
					std::string handle;
					ss.str(fstring);
					int i = 0;
					while (getline(ss, handle, '/'))
					{
						if (handle.length() > 0)
						{
							switch (i)
							{
							case 0:
								vertexIDs.push_back(atoi(handle.c_str()));
								triangleVerts.push_back(atoi(handle.c_str()));
								break;
							}
						}
						i++;
					}
					ss.clear();
				}

				if (triangleVerts.size() > 3)
				{
					int a = triangleVerts.at(0);
					int b = triangleVerts.at(1);
					int c = triangleVerts.at(2);
					int d = triangleVerts.at(3);

					if (abs(a - c) > abs(b - d))
					{
						std::vector<int> triangle1;
						triangle1.push_back(a - 1);
						triangle1.push_back(b - 1);
						triangle1.push_back(c - 1);

						std::vector<int> triangle2;
						triangle2.push_back(a - 1);
						triangle2.push_back(c - 1);
						triangle2.push_back(d - 1);

						triangles.push_back(triangle1);
						triangles.push_back(triangle2);
					}
					else
					{
						std::vector<int> triangle1;
						triangle1.push_back(a - 1);
						triangle1.push_back(b - 1);
						triangle1.push_back(d - 1);

						std::vector<int> triangle2;
						triangle2.push_back(d - 1);
						triangle2.push_back(b - 1);
						triangle2.push_back(c - 1);

						triangles.push_back(triangle1);
						triangles.push_back(triangle2);
					}
				}
				else
				{
					int a = triangleVerts.at(0);
					int b = triangleVerts.at(1);
					int c = triangleVerts.at(2);

					std::vector<int> triangle;
					triangle.push_back(a - 1);
					triangle.push_back(b - 1);
					triangle.push_back(c - 1);

					triangles.push_back(triangle);
				}
			}
		}
	}

	std::vector<Vector3> vertices;

	for (unsigned int i = 0; i < vertexIDs.size(); i++)
	{
		unsigned int vertexID = vertexIDs[i] - 1;
		Vector3 vertex = verticesLoaded[vertexID];
		vertices.push_back(vertex);
	}

	shared<Object> obj(new Object(verticesLoaded, triangles));
	return obj;
}