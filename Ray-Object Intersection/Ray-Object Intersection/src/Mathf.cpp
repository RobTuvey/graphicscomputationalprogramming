#include "Mathf.h"
#include "Vector3.h"
#include "Vector3d.h"

const float Mathf::pi = 3.14159265359f;

float Mathf::dotf(Vector3 a, Vector3 b)
{
	return ((a.x * b.x) + (a.y * b.y) + (a.z * b.z));
}

float Mathf::magnitudef(Vector3 vector)
{
	return sqrt((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));
}

float Mathf::shortestAnglef(Vector3 a, Vector3 b)
{
	float angleCos = dot(a, b) / magnitude(a) * magnitude(b);
	float angleRad = acos(angleCos);

	return angleRad * (180 / pi);
}

float Mathf::antiClockwiseAnglef(Vector3 a, Vector3 b, Vector3 c)
{
	float shortAngle = shortestAngle(a, b);
	
	float trip = triple(a, b, c);
	if (trip < 0)
		return 360 - shortAngle;

	return shortAngle;
}

float Mathf::clockwiseAnglef(Vector3 a, Vector3 b, Vector3 c)
{
	float shortAngle = shortestAngle(a, b);

	float trip = triple(a, b, c);
	if (trip > 0)
		return 360 - shortAngle;

	return shortAngle;
}

float Mathf::triplef(Vector3 a, Vector3 b, Vector3 c)
{
	return dot(c, cross(a, b));
}

double Mathf::dot(Vector3d a, Vector3d b)
{
	double d = ((a.x * b.x) + (a.y * b.y) + (a.z * b.z));

	return d;
}

double Mathf::magnitude(Vector3d vector)
{
	return sqrt((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));
}

double Mathf::shortestAngle(Vector3d a, Vector3d b)
{
	double angleCos = dot(a, b) / magnitude(a) * magnitude(b);
	double angleRad = acos(angleCos);

	return angleRad * (180 / pi);
}

double Mathf::antiClockwiseAngle(Vector3d a, Vector3d b, Vector3d c)
{
	double shortAngle = shortestAngle(a, b);

	double trip = triple(a, b, c);
	if (trip < 0)
		return 360 - shortAngle;

	return shortAngle;
}

double Mathf::clockwiseAngle(Vector3d a, Vector3d b, Vector3d c)
{
	double shortAngle = shortestAngle(a, b);

	double trip = triple(a, b, c);
	if (trip > 0)
		return 360 - shortAngle;

	return shortAngle;
}

double Mathf::triple(Vector3d a, Vector3d b, Vector3d c)
{
	return dot(c, cross(a, b));
}

Vector3 Mathf::normalize(Vector3 vector)
{
	float mag = magnitude(vector);

	return Vector3(vector.x / mag, vector.y / mag, vector.z / mag);
}

Vector3 Mathf::cross(Vector3 a, Vector3 b)
{
	return Vector3((a.y * b.z) - (a.z * b.y), (a.z * b.x) - (a.x * b.z), (a.x * b.y) - (a.y * b.x));
}

Vector3d Mathf::cross(Vector3d a, Vector3d b)
{
	Vector3 c = Vector3d((a.y * b.z) - (a.z * b.y), (a.z * b.x) - (a.x * b.z), (a.x * b.y) - (a.y * b.x));
	return c;
}