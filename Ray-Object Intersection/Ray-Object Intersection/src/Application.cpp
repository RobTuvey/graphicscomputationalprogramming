#include "Application.h"
#include "QuitOption.h"
#include "LoadObjectOption.h"
#include "Object.h"
#include "BruteForceOption.h"
#include <iostream>

shared<Context> Application::context;

void Application::init()
{
	context.reset(new Context());

	context->quit = false;
	context->outputText = "";
	context->outputOffset = 0;
	context->backCulling = true;
	context->cameraPos = Vector3(1.0f, 0.0f, 0.0f);
	context->bruteForceDirty = true;
	context->mollerTrumboreDirty = true;
}

void Application::run()
{
	while (!context->quit)
	{
		displayMenu();
		getInput();
	}
}

void Application::destroy()
{
	for (size_t i = 0; i < context->options.size(); i++)
	{
		context->options.erase(context->options.begin() + i);
		i--;
	}

	context.reset();
}

void Application::displayMenu()
{	
	system("CLS");
	std::cout << "  #################################" << std::endl;
	std::cout << " #    Ray-Object Intersections   #" << std::endl;
	std::cout << "#################################" << std::endl;
	std::cout << std::string(2, '\n').c_str();

	for (size_t i = 0; i < context->options.size(); i++)
	{
		std::cout << i + 1 << ". " << context->options.at(i)->getDisplayText() << std::endl;
	}

	std::cout << std::string(21 - context->options.size() - context->outputOffset, '\n');
	std::cout << context->outputText << std::endl;
	context->outputOffset = 0;
}

void Application::getInput()
{
	std::cout << std::endl << "Select an option: ";

	int input;
	std::cin >> input;
	if (std::cin.fail())
	{
		std::cout << "Sorry, that option was not recognized." << std::endl;
		std::cin.clear();
		std::cin.ignore(100, '\n');
	}
	else
	{
		if ((input - 1) < context->options.size())
		{
			context->options.at(input - 1)->action();
		}
		else
		{
			std::cout << "Sorry, that option was not recognized." << std::endl;
		}
	}
}