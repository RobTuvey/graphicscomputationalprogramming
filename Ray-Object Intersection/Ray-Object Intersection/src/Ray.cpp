#include "Ray.h"
#include "Mathf.h"
#include "Vector3d.h"

Ray::Ray()
{

}

Ray::Ray(Vector3 startVector, Vector3 directionVector)
{
	this->startVector = startVector;
	this->directionVector = directionVector;
}

Ray::~Ray()
{

}

void Ray::setStartVector(Vector3 vector)
{
	this->startVector = vector;
}

void Ray::setDirectionVector(Vector3 vector)
{
	this->directionVector = vector;
}

Ray Ray::generate(Vector3 startVector, Vector3 directionVector)
{
	return Ray(startVector, directionVector);
}

std::vector<shared<Ray>> Ray::generateRays()
{
	std::vector<shared<Ray>> rays;

	Vector3 d(1, 0, 0);

	for (int m = 0; m < 256; m++)
	{
		for (int n = 0; n < 256; n++)
		{
			shared<Ray> ray(new Ray(Vector3(0, m - 128, n - 128), d));
			rays.push_back(ray);
		}
	}

	return rays;
}

bool Ray::intersectsParallelTriangle(Ray ray, std::vector<Vector3> triangle, Vector3 &intersectionPoint)
{
	Vector3 A = triangle.at(0);
	Vector3 B = triangle.at(1);
	Vector3 C = triangle.at(2);
	//Define edge vectors for the triangle.
	Vector3 edge1 = B - A;
	Vector3 edge2 = C - A;

	//Test both edges, as long as one passes then we know parallel ray intersects with triangle.
	if (lineSegmentIntersection(ray, A, edge1, intersectionPoint))
		return true;
	else if (lineSegmentIntersection(ray, A, edge2, intersectionPoint))
		return true;
	
	return false;
}

bool Ray::lineSegmentIntersection(Ray ray, Vector3 lineStart, Vector3 lineDir, Vector3 &intersectionPoint)
{
	double t = distanceFromOrigin(ray, lineStart, lineDir);
	//Determines the point on the line where the ray will intersect.
	Vector3 point = lineStart + (lineDir * t);
	//Determines the distance from the line start of the intersection point and segment end.
	double intersectMag = Mathf::magnitude(point - lineStart);
	double lineMag = Mathf::magnitude(lineDir);
	//Check to determine if intersection point is within line segment.
	if (intersectMag < lineMag)
	{
		intersectionPoint = point;
		return true;
	}
	else return false; //Not within segment but intersects further up the line.
	
}

double Ray::distanceFromOrigin(Ray ray, Vector3 lineStart, Vector3 lineDir)
{
	//Determines the t component of the ray when it intersects with the line segment.
	Vector3 numerator = Mathf::cross(ray.getStartVector() - lineStart, ray.getDirectionVector());
	Vector3 denominator = Mathf::cross(lineDir, ray.getDirectionVector());

	double t = Mathf::magnitude(numerator) / Mathf::magnitude(denominator);

	return t;
}