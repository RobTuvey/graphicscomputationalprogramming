#include "MollerTrumbore.h"
#include "Ray.h"
#include "Application.h"
#include "Object.h"
#include "Mathf.h"
#include <iostream>
#include <string>
#include "Clock.h"

const float MollerTrumbore::epsilon = 0.0000001f;

void MollerTrumbore::run()
{
	Clock::takeTime(); //Take measurement for calculating processing time of operation.

	Application::context->mollerTrumboreLog.clear();
	int intersections = 0;
	int parallels = 0;
	std::vector<shared<Ray>> rays = Ray::generateRays();
	std::vector<std::vector<int>> objTriangles;
	//Retrieves full set of triangles if back culling is disabled or the culled triangles if it is enabled.
	if (!Application::context->backCulling) objTriangles = Application::context->object->getTriangles();
	else objTriangles = Application::context->object->cullBackFaces();
	std::vector<Vector3> vertices = Application::context->object->getVertices();

	for (size_t i = 0; i < objTriangles.size(); i++)
	{
		std::cout << "Triangle " << i << std::endl;
		//Retrieving the triangle vertices from the list of loaded vertices.
		std::vector<int> triangleIDs = objTriangles.at(i);
		std::vector<Vector3> triangle;
		triangle.push_back(vertices.at(triangleIDs.at(0)));
		triangle.push_back(vertices.at(triangleIDs.at(1)));
		triangle.push_back(vertices.at(triangleIDs.at(2)));

		for (size_t j = 0; j < rays.size(); j++)
		{
			Vector3 intersectionPoint;
			bool parallel = false;
			if (intersectsTriangle(*rays.at(j), triangle, intersectionPoint, parallel))
			{
				intersections++;
				if (parallel)
				{
					std::string log = "--Ray " + std::to_string(j) +
						" and Triangle " + std::to_string(i) + " are parallel";
					std::cout << log << std::endl;
					Application::context->mollerTrumboreLog.push_back(log);
					parallels++;
				}
				else {
					std::string log = "--Ray " + std::to_string(j) +
						" intersects Triangle " + std::to_string(i) + " at " +
						std::to_string(intersectionPoint.x) + ", " + std::to_string(intersectionPoint.y) +
						", " + std::to_string(intersectionPoint.z);
					std::cout << log << std::endl;
					Application::context->mollerTrumboreLog.push_back(log);
				}
			}
			else if (parallel)
			{
				std::cout << "--Ray " << j << " is parallel with triangle " << i << " and does not intersect." << std::endl;
				parallels++;
			}
		}
	}

	Application::context->mollerTrumboreLog.push_back("\n");
	Application::context->outputText =
		"Intersections detected: " + std::to_string(intersections) +
		"\nFaces: " + std::to_string(objTriangles.size()) +
		"\nRays: " + std::to_string(rays.size()) +
		"\nTime Taken: " + std::to_string(Clock::getResult()) + " s" +
		"\nIntersection points displayed above.";
	Application::context->mollerTrumboreLog.push_back(Application::context->outputText);

	Application::context->outputOffset = 5;

	Application::context->mollerTrumboreDirty = false;
}

bool MollerTrumbore::intersectsTriangle(Ray ray, std::vector<Vector3> triangle, Vector3& intersectionPoint, bool &parallel)
{
	Vector3 A = triangle.at(0);
	Vector3 B = triangle.at(1);
	Vector3 C = triangle.at(2);
	//Defines edge vectors of the triangle.
	Vector3 edge1 = B - A;
	Vector3 edge2 = C - A;
	//Gets the normal between the directional vector and the edge of the triangle.
	Vector3 h = Mathf::cross(ray.getDirectionVector(), edge2);
	//This can be checked to see if it is parallel by calculating the dot product.
	float det = Mathf::dotf(edge1, h);
	//If it is close to 0 then we know it is parallel and so no intersection.
	if (det > -epsilon && det < epsilon)
	{
		parallel = true;
		return false;
	}

	float f = 1 / det;
	//Determines the difference between the ray an the point A.
	Vector3 s = ray.getStartVector() - A;
	//Calculate u component of triangle coordinates
	float u = f * (Mathf::dotf(s, h));
	//Test bounds to determine if it is within the triangle.
	if (u < 0.0f || u > 1.0f)
		return false;
	//Returns the normal of the ray to triangle vector and the edge vector.
	Vector3 q = Mathf::cross(s, edge1);
	//Calculate v component of triangle coordinates.
	float v = f * Mathf::dotf(ray.getDirectionVector(), q);
	//Test bounds to determine if it is within the triangle.
	if (v < 0.0f || (v + u) > 1.0f)
		return false;
	//Now it is safe to calculate the t component of the ray as it is known
	//That the point is within the triangle.
	float t = f * Mathf::dotf(edge2, q);
	//Final check to make sure point is not behind the ray origin.
	if (t > epsilon)
	{
		intersectionPoint = ray.getStartVector() + (ray.getDirectionVector() * t);
		return true;
	}
	else return false;
}