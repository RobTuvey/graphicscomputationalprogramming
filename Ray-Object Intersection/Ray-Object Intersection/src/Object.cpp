#include "Object.h"
#include "Mathf.h"
#include "Application.h"
#include "Vector3d.h"

Object::Object()
{

}

Object::Object(std::vector<Vector3> vertices, std::vector<std::vector<int>> triangles)
{
	this->vertices = vertices;
	this->triangles = triangles;
}

Object::~Object()
{

}

void Object::setVertices(std::vector<Vector3> vertices)
{
	this->vertices = vertices;
}

void Object::setTriangles(std::vector<std::vector<int>> triangles)
{
	this->triangles = triangles;
}

std::vector<std::vector<int>> Object::cullBackFaces()
{
	std::vector<std::vector<int>> culledFaces;
	Vector3 camPos = Application::context->cameraPos;

	for (size_t i = 0; i < triangles.size(); i++)
	{
		std::vector<int> triangle = triangles.at(i);
		Vector3 A = vertices.at(triangle.at(0));
		Vector3 B = vertices.at(triangle.at(1));
		Vector3 C = vertices.at(triangle.at(2));

		//Calculates the coordinates of the centre point of the triangle.
		Vector3 triCentre = Vector3((A.x + B.x + C.x) / 3, (A.y + B.y + C.y) / 3, (A.z + B.z + C.z) / 3);
		//Defines edge vectors of the triangle.
		Vector3 edge1 = B - A;
		Vector3 edge2 = C - A;
		//Determines the normal of the triangle
		Vector3 planePart = Mathf::cross(edge1, edge2);
		//Returns a normalized vector of the surface normal to the triangle
		Vector3 planeNormal = planePart / Mathf::magnitudef(planePart);

		double dotProd = Mathf::dot(camPos, planeNormal);
		//If the resulting dot product is greater than or equal to 0 then it is facing in the same direction 
		//as the normal and so is not facing it.
		if (dotProd < 0) culledFaces.push_back(triangle);
	}

	return culledFaces;
}