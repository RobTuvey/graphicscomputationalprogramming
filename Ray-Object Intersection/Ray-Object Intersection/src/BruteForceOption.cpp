#include "BruteForceOption.h"
#include "Application.h"
#include "Object.h"
#include <iostream>
#include "BruteForce.h"

BruteForceOption::BruteForceOption()
{

}

BruteForceOption::BruteForceOption(std::string displayText, std::string outputText) : Option(displayText, outputText)
{

}

BruteForceOption::~BruteForceOption()
{

}

void BruteForceOption::action()
{
	if (Application::context->object != NULL)
	{
		BruteForce::run();
	}
	else
		Application::context->outputText = "A valid object has not been loaded.";
}