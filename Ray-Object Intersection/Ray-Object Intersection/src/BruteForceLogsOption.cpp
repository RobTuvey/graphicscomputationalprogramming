#include "BruteForceLogsOption.h"
#include "Application.h"
#include <iostream>

BruteForceLogsOption::BruteForceLogsOption()
{

}

BruteForceLogsOption::BruteForceLogsOption(std::string displayText, std::string outputText) : Option(displayText, outputText)
{

}

BruteForceLogsOption::~BruteForceLogsOption()
{

}

void BruteForceLogsOption::action()
{
	if (!Application::context->bruteForceDirty)
	{
		std::vector<std::string> log = Application::context->bruteForceLog;

		for (size_t i = 0; i < log.size(); i++)
		{
			std::string logCurrent = log.at(i);
			std::cout << logCurrent.c_str() << std::endl;
		}
		
		std::cin.clear();
		std::cin.ignore(100, '\n');
		std::string line;
		std::getline(std::cin, line);
		Application::context->outputText = "";
	}
	else
		Application::context->outputText = "Brute-Force logs are not up to date or incomplete.";
}