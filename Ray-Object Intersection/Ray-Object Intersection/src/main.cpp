#include <iostream>
#include "Application.h"
#include "LoadObjectOption.h"
#include "QuitOption.h"
#include "BruteForceOption.h"
#include "MollerTromboreOption.h"
#include "BackCullingOption.h"
#include "MollerTromboreLogsOption.h"
#include "BruteForceLogsOption.h"

int main(int argc, char* argv)
{
	Application::init();

	Application::addOption<LoadObjectOption>("Load OBJ", "OBJ Loaded");
	Application::addOption<BruteForceOption>("Geometrical Brute Force Method", "");
	Application::addOption<MollerTromboreOption>("Moller-Trombore Method", "");
	Application::addOption<BruteForceLogsOption>("Display Brute-Force Log", "");
	Application::addOption<MollerTromboreLogsOption>("Display Moller-Trumbore Log", "");
	Application::addOption<BackCullingOption>("Disable Back-Face Culling.", "");
	Application::addOption<QuitOption>("Exit the Application", "You have chosen to exit.");

	Application::run();

	Application::destroy();

	return 0;
}