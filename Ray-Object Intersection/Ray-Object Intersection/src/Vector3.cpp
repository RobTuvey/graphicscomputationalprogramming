#include "Vector3.h"
#include "Vector3d.h"
#include <iostream>

Vector3::Vector3()
{
	x = 0;
	y = 0;
	z = 0;
}

Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector3::Vector3(Vector3d v)
{
	this->x = v.x;
	this->y = v.y;
	this->z = v.z;
}

Vector3::Vector3(float f)
{
	x = f;
	y = f;
	z = f;
}

Vector3 Vector3::operator+ (Vector3 v)
{
	return Vector3(x + v.x, y + v.y, z + v.z);
}

Vector3 Vector3::operator- (Vector3 v)
{
	return Vector3(x - v.x, y - v.y, z - v.z);
}

Vector3 Vector3::operator-(Vector3d v)
{
	return Vector3(x - v.x, y - v.y, z - v.z);
}

Vector3 Vector3::operator* (Vector3 v)
{
	return Vector3(x * v.x, y * v.y, z * v.z);
}

Vector3 Vector3::operator+ (float f)
{
	return Vector3(x + f, y + f, z + f);
}

Vector3 Vector3::operator- (float f)
{
	return Vector3(x - f, y - f, z - f);
}

Vector3 Vector3::operator* (float f)
{
	return Vector3(x * f, y * f, z * f);
}

Vector3 Vector3::operator/ (float f)
{
	return Vector3(x / f, y / f, z / f);
}

Vector3 Vector3::operator+= (Vector3 v)
{
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;
	return *this;
}

Vector3 Vector3::operator= (Vector3* v)
{
	this->x = v->x;
	this->y = v->y;
	this->z = v->z;
	return *this;
}

Vector3 Vector3::operator= (Vector3 v)
{
	this->x = v.x;
	this->y = v.y;
	this->z = v.z;
	return *this;
}

Vector3 Vector3::rotateAroundPoint2D(Vector3 oVector, float angle, Vector3 point)
{
	float s = sin(angle);
	float c = cos(angle);

	oVector.x -= point.x;
	oVector.y -= point.y;

	float xNew = oVector.x * c - oVector.y * s;
	float yNew = oVector.x * s + oVector.y * c;

	oVector.x = xNew + point.x;
	oVector.y = yNew + point.y;

	return oVector;
}

float Vector3::magnitude(Vector3 v)
{
	return sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
}