#pragma once

#include <math.h>

class Vector3;
class Vector3d;

class Mathf
{
public:
	static float dotf(Vector3 a, Vector3 b);
	static float magnitudef(Vector3 vector);
	static float shortestAnglef(Vector3 a, Vector3 b);
	static float antiClockwiseAnglef(Vector3 a, Vector3 b, Vector3 c);
	static float clockwiseAnglef(Vector3 a, Vector3 b, Vector3 c);
	static float triplef(Vector3 a, Vector3 b, Vector3 c);

	static double dot(Vector3d a, Vector3d b);
	static double magnitude(Vector3d vector);
	static double shortestAngle(Vector3d a, Vector3d b);
	static double antiClockwiseAngle(Vector3d a, Vector3d b, Vector3d c);
	static double clockwiseAngle(Vector3d a, Vector3d b, Vector3d c);
	static double triple(Vector3d a, Vector3d b, Vector3d c);

	static Vector3 normalize(Vector3 vector);
	static Vector3 cross(Vector3 a, Vector3 b);
	static Vector3d cross(Vector3d a, Vector3d b);

	static const float pi;
};