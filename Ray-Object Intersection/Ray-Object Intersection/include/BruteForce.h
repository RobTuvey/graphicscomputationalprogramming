#pragma once

#include <vector>

class Vector3;
class Vector3d;

class BruteForce
{
public:
	static void run();
	static bool intersectsTriangle(Vector3 intersectionPoint, std::vector<Vector3> triangle, Vector3 planeNormal);
};