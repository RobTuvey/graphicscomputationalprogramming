#pragma once

#include "Option.h"
#include <windows.h>
#include <winnt.h>

class LoadObjectOption : public Option
{
public:
	LoadObjectOption();
	LoadObjectOption(std::string displayText, std::string outputText);
	~LoadObjectOption();
	
	void action();
private:
	OPENFILENAME objFile;
};