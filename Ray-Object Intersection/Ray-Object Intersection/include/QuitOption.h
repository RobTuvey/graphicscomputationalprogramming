#pragma once
#include "Option.h"

class QuitOption : public Option
{
public:
	QuitOption();
	QuitOption(std::string displayText, std::string outputText);
	~QuitOption();

	void action();
};