#pragma once

class Vector3;

class Vector3d
{
public:
	Vector3d();
	Vector3d(double x, double y, double z);
	Vector3d(Vector3 v);
	~Vector3d();

	double x;
	double y;
	double z;

	Vector3d operator+ (Vector3d v);
	Vector3d operator- (Vector3d v);
	Vector3d operator* (Vector3d v);

	Vector3d operator+ (double d);
	Vector3d operator- (double d);
	Vector3d operator* (double d);
	Vector3d operator/ (double d);

	Vector3d operator+= (Vector3d v);

	Vector3d operator= (Vector3d* v);
	Vector3d operator= (Vector3d v);
	Vector3d operator= (Vector3 v);

};