#pragma once

class Vector2
{
public:
	Vector2();
	Vector2(float x, float y);
	Vector2(float f);
	~Vector2() {}

	float x;
	float y;

	static float magnitude(Vector2 v);

	Vector2 operator+ (Vector2 v);
	Vector2 operator- (Vector2 v);
	Vector2 operator* (Vector2 v);
	Vector2 operator+ (float f);
	Vector2 operator- (float f);
	Vector2 operator* (float f);
	Vector2 operator/ (float f);

	Vector2 operator+= (Vector2 v);

	Vector2 operator= (Vector2* v);
	Vector2 operator= (Vector2 v);
private:
};