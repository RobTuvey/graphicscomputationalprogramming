#pragma once

#include <string>

class Option
{
public:
	Option();
	Option(std::string displayText, std::string outputText);
	~Option();

	void setDisplayText(std::string text) { this->displayText = text; }
	void setOutputText(std::string text) { this->outputText = text; }
	std::string getDisplayText() { return displayText; }
	std::string getOutputText() { return outputText; }

	virtual void action();

private:
	std::string displayText;
	std::string outputText;
};