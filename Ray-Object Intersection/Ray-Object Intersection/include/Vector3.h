#pragma once

class Vector3d;

class Vector3
{
public:
	Vector3();
	Vector3(float x, float y, float z);
	Vector3(Vector3d v);
	Vector3(float f);
	~Vector3() {}

	float x;
	float y;
	float z;

	static Vector3 rotateAroundPoint2D(Vector3 oVector, float angle, Vector3 point);
	static float magnitude(Vector3 v);

	Vector3 operator+ (Vector3 v);
	Vector3 operator- (Vector3 v);
	Vector3 operator- (Vector3d v);
	Vector3 operator* (Vector3 v);
	Vector3 operator+ (float f);
	Vector3 operator- (float f);
	Vector3 operator* (float f);
	Vector3 operator/ (float f);

	Vector3 operator+= (Vector3 v);

	Vector3 operator= (Vector3* v);
	Vector3 operator= (Vector3 v);
};