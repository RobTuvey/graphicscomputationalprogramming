#pragma once

#include "Common.h"
#include "Vector3.h"

class Option;
class Object;

struct Context
{
	bool quit = false;

	std::string outputText;
	int outputOffset;

	std::vector<shared<Option>> options;
	
	shared<Object> object;

	Vector3 cameraPos;
	bool backCulling;

	std::vector<std::string> bruteForceLog;
	std::vector<std::string> mollerTrumboreLog;

	bool bruteForceDirty;
	bool mollerTrumboreDirty;
};

class Application
{
public:
	static void init();
	static void run();
	static void destroy();

	template <typename T>
	static shared<T> addOption(std::string displayText, std::string outputText)
	{
		shared<T> option(new T(displayText, outputText));
		context->options.push_back(option);

		return option;
	}

	static shared<Context> context;
	
private:
	static void displayMenu();
	static void getInput();

};