#pragma once

#include "Option.h"

class MollerTromboreOption : public Option
{
public:
	MollerTromboreOption();
	MollerTromboreOption(std::string displayText, std::string outputText);
	~MollerTromboreOption();

	void action();
};