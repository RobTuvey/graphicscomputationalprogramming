#pragma once

#include <string>
#include "Common.h"

class Object;

class FileManager
{
public:
	static shared<Object> loadObj(std::string filename);
};