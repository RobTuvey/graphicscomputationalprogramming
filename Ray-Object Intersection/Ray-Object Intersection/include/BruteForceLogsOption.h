#pragma once

#include "Option.h"

class BruteForceLogsOption : public Option
{
public:
	BruteForceLogsOption();
	BruteForceLogsOption(std::string displayText, std::string outputText);
	~BruteForceLogsOption();

	void action();
};