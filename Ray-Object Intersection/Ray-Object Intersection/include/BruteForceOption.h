#pragma once

#include "Option.h"

class BruteForceOption : public Option
{
public:
	BruteForceOption();
	BruteForceOption(std::string displayText, std::string outputText);
	~BruteForceOption();

	void action();
};