#pragma once

#include "Common.h"

class Ray;
class Vector3;

class MollerTrumbore
{
public:
	static void run();
	static bool intersectsTriangle(Ray ray, std::vector<Vector3> triangle, Vector3& intersectionPoint, bool &parallel);

private:
	static const float epsilon;
};