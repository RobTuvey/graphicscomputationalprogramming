#pragma once

#include "Option.h"

class MollerTromboreLogsOption : public Option
{
public:
	MollerTromboreLogsOption();
	MollerTromboreLogsOption(std::string displayText, std::string outputText);
	~MollerTromboreLogsOption();

	void action();
};