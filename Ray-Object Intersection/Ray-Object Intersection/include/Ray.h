#pragma once

#include "Vector3.h"
#include <vector>
#include "Common.h"

class Ray
{
public:
	Ray();
	Ray(Vector3 startVector, Vector3 directionVector);
	~Ray();

	void setStartVector(Vector3 vector);
	void setDirectionVector(Vector3 vector);

	Vector3 getStartVector() { return startVector; }
	Vector3 getDirectionVector() { return directionVector; }

	static Ray generate(Vector3 startVector, Vector3 directionVector);
	static std::vector<shared<Ray>> generateRays();
	static bool intersectsParallelTriangle(Ray ray, std::vector<Vector3> triangle, Vector3 &intersectionPoint);
	static bool lineSegmentIntersection(Ray ray, Vector3 lineStart, Vector3 lineDir, Vector3 &intersectionPoint);
	static double distanceFromOrigin(Ray ray, Vector3 lineStart, Vector3 lineDir);
private:
	Vector3 startVector;
	Vector3 directionVector;
};