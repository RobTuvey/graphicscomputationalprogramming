#pragma once

#include <vector>
#include "Vector2.h"
#include "Vector3.h"

class Object
{
public:
	Object();
	Object(std::vector<Vector3> vertices, std::vector<std::vector<int>> triangles);
	~Object();

	void setVertices(std::vector<Vector3> vertices);
	void setTriangles(std::vector<std::vector<int>> triangles);

	std::vector<Vector3> getVertices() { return vertices; }
	std::vector<std::vector<int>> getTriangles() { return triangles; }

	std::vector<std::vector<int>> cullBackFaces();

private:
	std::vector<Vector3> vertices;
	std::vector<std::vector<int>> triangles;

};