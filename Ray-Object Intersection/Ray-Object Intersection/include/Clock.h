#pragma once

#include <chrono>
#include <iostream>

class Clock
{
public:
	static void takeTime();
	static float getResult();
private:
	static std::chrono::time_point<std::chrono::high_resolution_clock> measurePoint;
};