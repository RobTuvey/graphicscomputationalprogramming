#pragma once

#include "Option.h"

class BackCullingOption : public Option
{
public:
	BackCullingOption();
	BackCullingOption(std::string displayText, std::string outputText);
	~BackCullingOption();

	void action();

};